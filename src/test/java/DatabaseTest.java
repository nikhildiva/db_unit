import org.dbunit.DatabaseUnitException;
import org.dbunit.assertion.DbUnitAssert;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseTest {

	private IDatabaseConnection connection;

	@BeforeClass
	public void initiateContext() throws ClassNotFoundException, SQLException, DatabaseUnitException {
		System.out.println("Starting up");
		String schema = "test";
		// database connection
		Class.forName("com.mysql.jdbc.Driver");
		Connection jdbcConnection = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/test", "nikhilp", "nikhilp");
		connection = new DatabaseConnection(jdbcConnection, schema);
	}


	@Test(priority = 1)
	public void importTest() throws DatabaseUnitException, SQLException, IOException {
		System.out.println("Importing data");
		FlatXmlDataSet dataSet = new FlatXmlDataSet(new InputSource(new FileInputStream("src/main/resources/testdata/full.xml")));
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
	}

	@Test(priority = 2)
	public void exportTest() {
		System.out.println("Comparing exported data");
		DbUnitAssert dbUnitAssert = new DbUnitAssert();

		// full database export
		IDataSet fullDataSet = null;
		try {
			fullDataSet = connection.createDataSet();
			FlatXmlDataSet.write(fullDataSet, new FileOutputStream("full.xml"));

			FlatXmlDataSet expectedSet = new FlatXmlDataSet(new File("src/main/resources/testdata/full.xml"));
			FlatXmlDataSet actualSet = new FlatXmlDataSet(new File("full.xml"));

			dbUnitAssert.assertEquals(expectedSet, actualSet);
		} catch (SQLException | IOException | DatabaseUnitException e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public void cleanup() throws DatabaseUnitException, SQLException, IOException {
		System.out.println("Cleaning up");
		FlatXmlDataSet dataSet = new FlatXmlDataSet(new InputSource(new FileInputStream("src/main/resources/testdata/full.xml")));
		DatabaseOperation.DELETE.execute(connection, dataSet);
	}
}

